## Build and run

```
docker build -t remyguillaume/gmf-proxy .
docker push remyguillaume/gmf-proxy
docker run --rm --env GMF_HOST=map.geo.bs.ch --name=haproxy -p 9000:9000 remyguillaume/gmf-proxy
```

## Create Let's encrypt certificate

```
apt update
apt install snapd
snap install core
snap refresh core
snap install --classic certbot
ln -s /snap/bin/certbot /usr/bin/certbot
certbot certonly --standalone

cp /etc/letsencrypt/live/pi.paloo.fr/fullchain.pem haproxy/fullchain.pem
chown pi:pi haproxy/fullchain.pem 
cp /etc/letsencrypt/live/pi.paloo.fr/privkey.pem haproxy/
chown pi:pi haproxy/privkey.pem 

cat fullchain.pem privkey.pem > pi.paloo.fr.pem
```

## 
Renew Certificate

```
certbot renew
cat fullchain.pem privkey.pem > pi.paloo.fr.pem
```